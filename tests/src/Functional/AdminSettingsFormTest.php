<?php

declare(strict_types = 1);

namespace Drupal\Tests\safedelete\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\node\Entity\NodeType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests admin settings form.
 *
 * @group safedelete
 */
class AdminSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'safedelete',
    'node',
  ];

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * The another user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $anotherUser;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'safedelete administration',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Sets the test up.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser($this->permissions);
    $this->anotherUser = $this->drupalCreateUser();
  }

  /**
   * Test that admin user can update the admin settings via admin settings page.
   */
  public function testAdminSettingsForm() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);
    $this->createTestContentType();
    $this->drupalGet('/admin/config/development/safedelete');
    $assert_session->statusCodeEquals(Response::HTTP_OK);
    $edit = [
      'delete_button' => TRUE,
      'limit' => 2
    ];
    $this->submitForm($edit, 'Save configuration');
    $assert_session->statusCodeEquals(Response::HTTP_OK);
    $this->drupalGet('/admin/content/safedelete-orphanedpages');
    $assert_session->statusCodeEquals(Response::HTTP_OK);
  }

  /**
   * Test that non-admin user can't access the admin settings page.
   */
  public function testAdminSettingsFormForbiddenAccess() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->anotherUser);
    $this->drupalGet('/admin/config/development/safedelete');
    $assert_session->statusCodeEquals(Response::HTTP_FORBIDDEN);
  }

  /**
   * Helper function to create a test content type.
   */
  protected function createTestContentType() {
    $type = NodeType::create([
      'type' => 'test_bundle',
      'name' => 'Test Bundle',
    ]);
    $type->save();

    // Grant permissions to the admin user for the new content type.
    $role = \Drupal\user\Entity\Role::load('authenticated');
    if (!$role->hasPermission('administer content types')) {
      $role->grantPermission('administer content types');
      $role->grantPermission('create test_bundle content');
      $role->grantPermission('edit any test_bundle content');
      $role->grantPermission('delete any test_bundle content');
      $role->grantPermission('safedelete view orphans report');
      $role->grantPermission('safedelete create orphans report');
      $role->grantPermission('safedelete administration');
      $role->save();
    }
    $this->assertTrue($role->hasPermission('administer content types'), 'Permission successfully granted.');
  }
}
