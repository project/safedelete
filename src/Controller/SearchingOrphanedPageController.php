<?php

namespace Drupal\safedelete\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\safedelete\AdminHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class SearchingOrphanedPageController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new SearchingOrphanedPageController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\State\State $state
   *   The state service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   */
  public function __construct(Connection $database,
  RouteMatchInterface $route_match,
  State $state,
   FileSystemInterface $file_system,
  LanguageManagerInterface $language_manager,
  AccountInterface $current_user) {
    // parent::__construct($configuration, $plugin_id, $plugin_definition);.
    $this->database = $database;
    $this->routeMatch = $route_match;
    $this->state = $state;
    $this->fileSystem = $file_system;
    $this->languageManager = $language_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Container create method.
   */
  public static function create(ContainerInterface $container) {
    $database = $container->get('database');
    return new static(
      $database,
      $container->get('current_route_match'),
      $container->get('state'),
      $container->get('file_system'),
      $container->get('language_manager'),
      $container->get('current_user')
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    $viewreport = FALSE;
    $rountingname = $this->routeMatch->getRouteName();
    if ($this->routeMatch->getRouteName() == 'safedelete_orphanedpages.content') {
      if ($this->currentUser->isAuthenticated()) {
        // Was doing this to eliminate admin rendering but template changes should have stripped most of that out.
        // Uncomment the two lines below if we want to force a logout.
        // $session_manager = \Drupal::service('session_manager');
        // $session_manager->delete(\Drupal::currentUser()->id());
      }
    }
    if ($this->routeMatch->getRouteName() == 'safedelete_vieworphanedpagesreport.admin_content') {
      $viewreport = TRUE;
    }
    $vars_array = [];
    $vars_array['config_url'] = Url::fromRoute('safedelete.settings')->toString();
    $vars_array['system_cron_url'] = Url::fromRoute('system.cron', ['key' => $this->state->get('system.cron_key')], ['absolute' => TRUE])->toString();
    return [
      '#theme' => 'safedelete_orphanedpages',
      '#showconfigsettinglink' => $this->showconfigsettinglink(),
      '#orphanedpages_container' => $this->getOrphanedLandingPageOrBasicPage($viewreport),
      '#vars_array' => $vars_array,
    ];
  }

  /**
   * Get orphaned pages.
   */
  public function getOrphanedLandingPageOrBasicPage($viewreport = FALSE) {
    $reporteddate = '';
    $orphanedpages_results = [];
    $filedirectory = 'private://orphaned_pages_files/';

    // Read the report from file system.
    if ($viewreport) {
      $directoryexists = $this->fileSystem->prepareDirectory($filedirectory);
      if (!$directoryexists) {
        $this->fileSystem->mkdir($filedirectory);
      }
      else {
        $options['key'] = "name";
        $files = $this->fileSystem->scanDirectory($filedirectory, '/.json/');
        $filearraynid = [];
        foreach ($files as $file) {
          $nodeidstr = $file->name;
          $nodeid = (int) $nodeidstr;
          $filesarraynid[$nodeid] = $file;
        }
        if (isset($filesarraynid)) {
          ksort($filesarraynid);
          $langcode = $this->languageManager->getCurrentLanguage()->getId();
          foreach ($filesarraynid as $key => $file) {
            $uri = $file->uri;
            $buffer = file_get_contents($uri);
            $jsondata = json_decode($buffer);
            $nid = $jsondata->nid;
            $reporteddate = $jsondata->reporteddate;
            $orphanedpages_results[$nid]['nid'] = $nid;
            $orphanedpages_results[$nid]['type'] = $jsondata->type;
            $orphanedpages_results[$nid]['href'] = AdminHelper::getUrlForNode($nid, $langcode);
            $orphanedpages_results[$nid]['title'] = $jsondata->title;
            $orphanedpages_results[$nid]['reason'] = $jsondata->reason;
            $orphanedpages_results[$nid]['reporteddate'] = $reporteddate;
          }
        }
      }
    }
    else {
      // Generate report.
      AdminHelper::listOrphanedNodes($reporteddate, $orphanedpages_results, $filedirectory, $this->database);
    }
    $finalresult = [
      'reporteddate' => $reporteddate,
      'orphanedpages_results' => $orphanedpages_results,
    ];
    return $finalresult;
  }

  public function showconfigsettinglink() {
    // Check if the user has the "safedelete administration" permission.
    $user = $this->currentUser;
    $has_permission = $user->hasPermission('safedelete administration');
    return $has_permission;
  }

}
