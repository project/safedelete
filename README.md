Safe Delete
 Installation:

To install this module, please run the following command in your project's root directory:

```bash
composer require ezyang/htmlpurifier
```


  Also, it is recommended to use this core patch with safedelete:
  https://www.drupal.org/files/issues/2019-08-31/3027240-12.patch

 New to safe delete:  Safe Archive!
  Changing to archived moderation state now triggers validation logic similar/
  same as safe delete.

 This module is designed to do validation to prevent deleting content
 that has referenced links created by linkit.
 Other link types /reference types will be supported in later versions.
 This module is built to prevent broken links caused by deleting
 content that is linked in other nodes body fields.

 Features

  This feature can  be enabled or disabled  for particular bundle
  using config form.
  Admin can limit the number of records to show on node delete form.
  Admin can disable or enable delete button in node delete form,
  when disabled admin cannot be able to
  delete the node as it is associated with the entities, when enabled admin
  or user can delete even though when the node is associated with links
  in body fields that will be broken
